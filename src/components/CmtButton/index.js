import { useDispatch, useSelector } from "react-redux";
import { useState } from 'react'
import { addCommentThunk } from "../../store/modules/user/thunk"
import { Button, Input, Grid } from '@material-ui/core'


function CmtButton() {

    const dispatch = useDispatch()
    const test = useSelector(state => state.user)
    const [comment, setComment] = useState("")

    const postComment = (string) => {
        dispatch(addCommentThunk(string))
    }

    console.log(test)

    return (
        <>
            <Input onChange={(e) => setComment(e.target.value)} label="Comentário" />
            <Button variant="contained" color="primary"
                onClick={() => postComment(comment)}>Comment</Button>
            <Grid container>
                {test.comments?.map(x => <Grid item xs={12}><p>{x}</p></Grid>)}
            </Grid>
        </>
    )

}

export default CmtButton
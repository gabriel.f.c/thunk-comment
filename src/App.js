import CmtButton from './components/CmtButton';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <CmtButton/>
      </header>
    </div>
  );
}

export default App;
